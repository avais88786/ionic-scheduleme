import { Component, AfterViewInit, ViewChild, ElementRef, OnInit, ChangeDetectorRef } from '@angular/core';
import { Platform, Menu, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { LocalNotifications, ILocalNotification } from '@ionic-native/local-notifications';
import { DataServiceProvider } from '../providers/data-service/data-service';
import { NotificationSettings } from '../models/storage';
import { NotificationServiceProvider } from '../providers/notification-service/notification-service';
import { Subject } from '../../node_modules/rxjs';

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements AfterViewInit, OnInit {
  @ViewChild('menu') menu: Menu;
  rootPage: any = TabsPage;

  originalNotificationObject: NotificationSettings;
  todoReminderDaysBefore: number;
  todoReminderTime: string;
  doingInterval: string;
  enableNotifications: boolean;

  constructor(private platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private dataServiceProvider: DataServiceProvider,
    private changeDetectionRef: ChangeDetectorRef,
    private app: App,
    private notificationServiceProvider: NotificationServiceProvider
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleBlackTranslucent();
      splashScreen.hide();

      platform.registerBackButtonAction(() => {

        let nav = app.getActiveNavs()[0];
        let activeView = nav.getActive();

        if (!nav.canGoBack()) {
          alert("avais");
        }

      });

    });
  }

  ngOnInit(): void {
    this.dataServiceProvider.todoListHandler.subscribe(storageModel => {
      this.todoReminderDaysBefore = storageModel.notificationSettings.TODO_Reminder;
      this.doingInterval = storageModel.notificationSettings.DOING_Interval;
      this.enableNotifications = storageModel.notificationSettings.enableNotifications;
      this.originalNotificationObject = storageModel.notificationSettings;
      let time1 = storageModel.notificationSettings.TODOReminderhour.toString().length == 1 ? `0${storageModel.notificationSettings.TODOReminderhour}` : storageModel.notificationSettings.TODOReminderhour;
      let time2 = storageModel.notificationSettings.TODOReminderMinutes.toString().length == 1 ? `0${storageModel.notificationSettings.TODOReminderMinutes}` : storageModel.notificationSettings.TODOReminderMinutes;
      this.todoReminderTime = `${time1}:${time2}`;
    });
  }

  ngAfterViewInit(): void {
    var that = this;
    this.platform.ready().then(() => {
      let topNavbarHeight = this.menu.getNativeElement().parentElement.querySelector('ion-nav ng-component ion-tabs ion-header').clientHeight;
      let bottomTabHeight = this.menu.getNativeElement().parentElement.querySelector('ion-nav ng-component ion-tabs div').clientHeight;
      this.menu.getElementRef().nativeElement.style.marginTop = `${topNavbarHeight}px`;
      this.menu.getElementRef().nativeElement.style.marginBottom = `${bottomTabHeight}px`;
    });
  }

  menuClosed() {
    let todoReminderSplit = this.todoReminderTime.split(':');
    this.dataServiceProvider.updateIntervals(this.enableNotifications,
      this.todoReminderDaysBefore,
      this.doingInterval,
      parseInt(todoReminderSplit[0]),
      parseInt(todoReminderSplit[1]));
    this.dataServiceProvider.todoListHandler.subscribe(storageModel => {
      this.notificationServiceProvider.scheduleNotifications(storageModel);
    }).unsubscribe();
  }

  notify() {
    if (!this.enableNotifications) {
      this.changeDetectionRef.detectChanges();
      setTimeout(() => {
        this.doingInterval = this.originalNotificationObject.DOING_Interval;
        this.todoReminderDaysBefore = this.originalNotificationObject.TODO_Reminder;
        this.todoReminderTime = `${this.originalNotificationObject.TODOReminderhour}:${this.originalNotificationObject.TODOReminderMinutes}`;
      }, 0);
    }
  }
}
