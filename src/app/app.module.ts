import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { ComponentsModule } from '../components/components.module';
import { DataServiceProvider } from '../providers/data-service/data-service';
import { ToDoPage } from '../pages/todo/todo';
import { DoingPage } from '../pages/doing/doing';
import { DonePage } from '../pages/done/done';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { NotificationServiceProvider } from '../providers/notification-service/notification-service';
import { CalendarModule } from "ion2-calendar";
import { DatePipe } from '@angular/common';
import { LoaderServiceProvider } from '../providers/loader-service/loader-service';

@NgModule({
  declarations: [
    MyApp,
    DonePage,
    DoingPage,
    ToDoPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(MyApp, {
      platforms: {
        android: {
          activator: 'none'
        }
      }
    }),
    IonicStorageModule.forRoot(),
    ComponentsModule,
    CalendarModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DonePage,
    DoingPage,
    ToDoPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    DataServiceProvider,
    LocalNotifications,
    NotificationServiceProvider,
    DatePipe,
    LoaderServiceProvider
  ]
})
export class AppModule { }
