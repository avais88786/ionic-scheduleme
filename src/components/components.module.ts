import { NgModule } from '@angular/core';
import { PopoverItemActionsComponent } from './popover-item-actions/popover-item-actions';
import { IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { ItemsListComponent } from './items-list/items-list';
import { CalendarModule } from 'ion2-calendar';
import { AppHeaderComponent } from './app-header/app-header';
@NgModule({
	declarations: [PopoverItemActionsComponent,
    ItemsListComponent,
    AppHeaderComponent],
	imports: [
		IonicModule.forRoot(PopoverItemActionsComponent),
		CalendarModule
	],
	exports: [PopoverItemActionsComponent,
    ItemsListComponent,
    AppHeaderComponent],
	entryComponents: [
		PopoverItemActionsComponent]
})
export class ComponentsModule {}
