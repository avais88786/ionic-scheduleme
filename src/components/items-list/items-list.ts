import { Component, Input, EventEmitter, Output, OnChanges, SimpleChanges } from '@angular/core';
import { Item } from '../../models/item';
import { trigger, transition, useAnimation } from '@angular/animations';
import { dropAnimation, itemLeftAnimation } from '../../providers/animations/animations';
import { DateTime, ModalController } from 'ionic-angular';
import { CalendarModalOptions, CalendarModal, CalendarResult } from 'ion2-calendar';

/**
 * Generated class for the ItemsListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'items-list',
  templateUrl: 'items-list.html',
  animations: [
    trigger('itemLeft', [
      transition(':leave', [
        useAnimation(itemLeftAnimation)
      ])
    ])
  ]
})
export class ItemsListComponent implements OnChanges {

  @Input() todoList: Array<Item> = new Array<Item>();
  @Output() selectedItem: EventEmitter<Item> = new EventEmitter<Item>();
  @Output() deadlineChanged: EventEmitter<Item> = new EventEmitter<Item>();
  @Input() itemRemoved: boolean = false;
  @Input() itemMoved: boolean = false;



  x_2: string = "translateX(30%)";
  x_3: string = "translateX(50%)";
  x_4: string = "translateX(80%)";

  changedDeadline: string = null;
  changeDeadlineTrigerred = false;
  constructor(private modelCtrl: ModalController) {
  }

  itemClicked(item) {
    this.selectedItem.emit(item);
  }

  ngOnChanges(changes: SimpleChanges): void {
    for (let change in changes) {
      if (change === 'itemRemoved' && changes[change].currentValue == true) {
        this.x_2 = "translateX(-30%)";
        this.x_3 = "translateX(-50%)";
        this.x_4 = "translateX(-80%)";
      } else if (change === 'itemMoved' && changes[change].currentValue == true) {
        this.x_2 = "translateX(30%)";
        this.x_3 = "translateX(50%)";
        this.x_4 = "translateX(80%)";
      }
    }
  }

  changeDeadline(event, item: Item, datePicker: DateTime) {
    event.stopPropagation();
    const options: CalendarModalOptions = {
      title: `Change Deadline - ${item.todoItem}`,
      defaultDate: item.todoDeadline
    };
    let myCalendar = this.modelCtrl.create(CalendarModal, {
      options: options
    });


    myCalendar.present();

    myCalendar.onDidDismiss((date: CalendarResult, type: string) => {
      if (date) {
        this.dateChangedCallback(date.dateObj, item)
      }
    })
  }

  dateChangedCallback(event : Date, item: Item) {
      item.todoDeadline = event;
      this.deadlineChanged.emit(item);
      this.changeDeadlineTrigerred = false;
  }
}
