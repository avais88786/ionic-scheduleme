import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { DataServiceProvider } from '../../providers/data-service/data-service';

@Component({
  selector: 'popover-item-actions',
  templateUrl: 'popover-item-actions.html'
})
export class PopoverItemActionsComponent {

  todoItem:string;
  showDatePckr:boolean = false;

  constructor(private navParams:NavParams,
              private dataServiceProvider: DataServiceProvider,
              private viewController:ViewController) {
    this.todoItem = navParams.get('item');
  }

  removeItem(){
    let todoList: Array<string>;
    //this.dataServiceProvider.removeToDoItem((this.todoItem));
    this.viewController.dismiss();    
  }

  showDatePicker() {
    this.showDatePckr = !this.showDatePckr;
  }
}
