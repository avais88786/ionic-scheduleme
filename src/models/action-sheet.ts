import { Item } from "./item";
import { DataServiceProvider } from "../providers/data-service/data-service";
import { ITEM_STATUS } from "./todo-status";
import { ActionSheetButton } from "ionic-angular";

export class ActionSheet {

    item: Item;
    dataServiceProvider: DataServiceProvider;

    constructor(item: Item, dataServiceProvider: DataServiceProvider) {
        this.item = item;
        this.dataServiceProvider = dataServiceProvider;
    }

    public getToDoActionSheet(doingActionHandler: Function, removedItemActionHandler?: Function, editTextActionHandler?: Function) {
        return [
            {
                text: 'Edit',
                icon: 'create',
                handler: () => {
                    if (editTextActionHandler) {
                        editTextActionHandler.apply(null);
                    }
                }
            },
            this.getRemoveButton(this.item, removedItemActionHandler), 
            this.getNextAction(this.item, doingActionHandler, 'Start Doing!'), 
            //this.getChangeDeadlineButton(this.item), 
            this.getCancelButton(this.item)]
    }


    public getDoingActionSheet(doneActionHandler: Function, removedItemActionHandler?: Function) {
        return [this.getRemoveButton(this.item, removedItemActionHandler),
        {
            text: 'Start Again',
            icon: 'rewind',
            handler: () => {

                if (removedItemActionHandler) {
                    removedItemActionHandler.apply(null);
                }

                setTimeout(() => {
                    let clonedItem: Item = Object.assign({}, this.item);
                    clonedItem.todoStatus = ITEM_STATUS.TODO;
                    this.dataServiceProvider.removeToDoItem(this.item);
                    this.dataServiceProvider.addTodoItem(clonedItem);
                }, 100);
            }
        },
        this.getNextAction(this.item, doneActionHandler, 'Done'), 
        //this.getChangeDeadlineButton(this.item), 
        this.getCancelButton(this.item)]
    }

    private getRemoveButton(item: Item, removedItemActionHandler?: Function): ActionSheetButton {
        return {
            text: 'Remove',
            role: 'destructive',
            icon: 'trash',
            cssClass: 'caution',
            handler: () => {
                if (removedItemActionHandler) {
                    removedItemActionHandler.apply(null);
                }
                setTimeout(() => {
                    this.dataServiceProvider.removeToDoItem(item);
                }, 100)

            }
        };
    }

    private getChangeDeadlineButton(item: Item): ActionSheetButton {
        return {
            text: 'Change Deadline',
            icon: 'alarm',
            handler: () => {
                console.log('Deadline Change');
            }
        };
    }

    private getCancelButton(item: Item): ActionSheetButton {
        return {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'normal'
        };
    }

    private getNextAction(item: Item, nextActionHandler: Function, text: string): ActionSheetButton {
        return {
            text: text,
            icon: 'open',
            handler: () => nextActionHandler()
        }
    }
}