import { Item } from "./item";
import { StorageModel } from "./storage";

export interface iNotificationScheduler {
    scheduleNotifications(storageModel : StorageModel);
}