import { ITEM_STATUS } from "./todo-status";

export class Item {
    id:string;
    todoItem:string = null;
    todoDeadline:Date = null;
    todoStatus:ITEM_STATUS;
    todoNotes:string = "null";
}