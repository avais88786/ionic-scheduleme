import { iNotificationScheduler } from "./iNotification-scheduler";
import { Item } from "./item";
import { LocalNotifications, ILocalNotification, ILocalNotificationTrigger, ELocalNotificationTriggerUnit } from "@ionic-native/local-notifications";
import { NOTIFICATION_TODO } from "./constants";
import { ITEM_STATUS } from "./todo-status";
import { Pipe } from '@angular/core';
import { from } from "rxjs/observable/from";
import { groupBy, mergeMap, toArray } from "rxjs/operators";
import { Observable } from "rxjs";
import { observeOn } from "rxjs/operator/observeOn";
import { DateTime } from "ionic-angular";
import * as rn from "random-number";
import { StorageModel } from "./storage";

class DeadlineGroup {
    items: Array<Item> = new Array<Item>();
    deadlineDate: Date;
}

class ScheduleGroup {
    //deadlineDate: Date;
    scheduleDate: Date;
    notificationText: String[];
    isInPast: boolean = false;;

    constructor(notificationText?: string[], scheduleDate?: Date) {
        this.scheduleDate = scheduleDate;
        this.notificationText = notificationText;
    }
}


export class NotificationScheduler implements iNotificationScheduler {

    public DEADLINE_WINDOW_TODO: number = 2;
    readonly NOTIFICATION_TODOHEADER = 'Make a start on';
    readonly NOTIFICATION_DOINGHEADER = "Reminder";

    randomNumberOptions = {
        min: 1,
        max: 1000,
        integer: true
    }

    constructor(private localNotifications: LocalNotifications) {

    }

    scheduleNotifications(storageModel: StorageModel) {

        let items = storageModel.items;
        if (!items || (items && items.length == 0)) {
            return;
        }
        this.DEADLINE_WINDOW_TODO = storageModel.notificationSettings.TODO_Reminder;

        let todoItems: Array<Item> = items.filter(x => x.todoStatus == ITEM_STATUS.TODO && x.todoDeadline);
        let doingItems: Array<Item> = items.filter(x => x.todoStatus == ITEM_STATUS.DOING && x.todoDeadline);

        if (todoItems.length > 0) {

            let clonedItems: Array<Item> = Array.from(todoItems).sort((i1, i2) => i1.todoDeadline.getTime() - i2.todoDeadline.getTime());
            let groupedByDeadline: Array<DeadlineGroup> = this.groupBy(clonedItems, "todoDeadline");
            let scheduleGroups: Array<ScheduleGroup> = new Array<ScheduleGroup>();

            groupedByDeadline.forEach((deadlineGroup, index) => {
                let deadline: Date = deadlineGroup.deadlineDate;

                let notificationText: string[] = deadlineGroup.items
                    .map(x => {
                        return x.todoItem;
                    });

                if (new Date().getTime() > deadline.getTime()) {

                    let scheduleGroup = scheduleGroups.find(sg => sg.isInPast == true);

                    if (scheduleGroup == null) {
                        scheduleGroup = new ScheduleGroup(notificationText);
                        scheduleGroup.isInPast = true;
                        scheduleGroups.push(scheduleGroup);
                        return;
                    }
                    scheduleGroup.notificationText = [...scheduleGroup.notificationText, ...notificationText];
                }
                else {
                    for (let i = this.DEADLINE_WINDOW_TODO; i >= 0; i--) {

                        let schedulerDate: Date = new Date(deadline);
                        schedulerDate.setDate(deadline.getDate() - i);

                        let nowDateTime: Date = new Date();

                        if (nowDateTime.getTime() > schedulerDate.getTime()) { //if deadline is in past...
                            //                       continue;
                            schedulerDate = new Date();
                            // set to todays date or tomorrow depending on if it is 8oclock yet
                            schedulerDate.setDate((nowDateTime.getHours() <= storageModel.notificationSettings.TODOReminderhour
                                && nowDateTime.getMinutes() < storageModel.notificationSettings.TODOReminderMinutes) ? nowDateTime.getDate() : nowDateTime.getDate() + 1);
                        }

                        let scheduleGroup = scheduleGroups.find(sg => sg.scheduleDate != null && sg.scheduleDate.getDate() == schedulerDate.getDate()
                            && sg.scheduleDate.getMonth() == schedulerDate.getMonth()
                            && sg.scheduleDate.getFullYear() == schedulerDate.getFullYear());

                        if (scheduleGroup == null) {
                            scheduleGroup = new ScheduleGroup(notificationText, schedulerDate);
                            scheduleGroups.push(scheduleGroup);
                            continue;
                        }

                        scheduleGroup.notificationText = [...scheduleGroup.notificationText, ...notificationText];
                    }
                }


            });

            scheduleGroups.forEach(group => {
                let randomNum: number = rn(this.randomNumberOptions);
                let text: string = Array.from(new Set(group.notificationText)).join('\n');

                let notification;
                if (!group.isInPast) {
                    let id = Math.floor(group.scheduleDate.getTime() / randomNum);
                    notification = this.getFixedDateNotificationObject(id, this.NOTIFICATION_TODOHEADER, text, group.scheduleDate, storageModel, ITEM_STATUS.TODO);
                } else {
                    notification = this.getRepeatingNotificationObject(randomNum, this.NOTIFICATION_TODOHEADER, text, storageModel, ITEM_STATUS.TODO);
                }

                //(<any>window).cordova.plugins.notification.local.schedule(notification);
            })
        }

        if (doingItems.length > 0) {

            let doingText: string[] = doingItems.map(x => {
                return x.todoItem;
            });

            let text = doingText.join('\n');
            let randomNum: number = Math.floor(new Date().getTime() / rn(this.randomNumberOptions));
            let notification = this.getRepeatingNotificationObject(randomNum, this.NOTIFICATION_DOINGHEADER, text, storageModel, ITEM_STATUS.DOING);
            //(<any>window).cordova.plugins.notification.local.schedule(notification);
        }
    }

    getFixedDateNotificationObject(id: number, notificationHeader: string, text: string, scheduleDate: Date, storageModel: StorageModel, status: ITEM_STATUS) {
        return {
            id: id,
            title: `${notificationHeader}`,
            text: text,
            trigger: {
                at: new Date(scheduleDate.getFullYear(),
                    scheduleDate.getMonth(),
                    scheduleDate.getDate(),
                    storageModel.notificationSettings.TODOReminderhour,
                    storageModel.notificationSettings.TODOReminderMinutes,
                    0)
            },
            icon: status == ITEM_STATUS.TODO ? 'res://notification_todo' : 'res://notification_doing',
            foreground: false,
            color: status == ITEM_STATUS.TODO ? '#FF0000' : '#008000',
            smallIcon: 'res://smallicon'
        }
    }


    getRepeatingNotificationObject(id: number, notificationHeader: string, text: string, storageModel: StorageModel, status: ITEM_STATUS): any {
        return {
            id: id,
            title: `${notificationHeader}`,
            text: text,
            trigger: {
                every: {
                    hour: storageModel.notificationSettings.TODOReminderhour,
                    minute: storageModel.notificationSettings.TODOReminderMinutes
                }
            },
            icon: status == ITEM_STATUS.TODO ? 'res://notification_todo' : 'res://notification_doing',
            foreground: false,
            color: status == ITEM_STATUS.TODO ? '#FF0000' : '#008000',
            smallIcon: 'res://smallicon'
        };
    }

    groupBy(array: Array<Item>, key: string) {

        return array.reduce(function (groups, item) {
            const val: Date = item[key];
            let group = groups.find(x => x.deadlineDate.getTime() == val.getTime());
            if (group == null) {
                group = new DeadlineGroup();
                group.deadlineDate = val;
                group.items.push(item);
                groups.push(group);
                return groups;
            }
            group.items.push(item);
            return groups;
        }, Array<DeadlineGroup>());
    }
}