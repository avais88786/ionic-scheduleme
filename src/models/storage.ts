import { Item } from "./item";

export class StorageModel {
    public notificationSettings: NotificationSettings = new NotificationSettings();
    public items: Array<Item> = new Array<Item>();
}

export class NotificationSettings {
    public TODO_Reminder: number = 3;
    public TODOReminderhour: number = 8;
    public TODOReminderMinutes: number = 0;
    public DOING_Interval: string = "1";
    public enableNotifications = true;
}