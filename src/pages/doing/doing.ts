import { Component, OnInit } from '@angular/core';
import { NavController, ActionSheetController } from 'ionic-angular';
import { DataServiceProvider } from '../../providers/data-service/data-service';
import { ITEM_STATUS } from '../../models/todo-status';
import { Item } from '../../models/item';
import { ActionSheet } from '../../models/action-sheet';
import { trigger, transition, useAnimation } from '@angular/animations';
import { itemLeftAnimation } from '../../providers/animations/animations';

@Component({
  selector: 'page-doing',
  templateUrl: 'doing.html'
})
export class DoingPage implements OnInit {

  todoList: Array<Item> = new Array<Item>();
  itemRemoved: boolean = false;
  itemMoved: boolean = false;

  constructor(public navCtrl: NavController,
    private dataServiceProvider: DataServiceProvider,
    public actionSheetCtrl: ActionSheetController) {

  }

  ngOnInit(): void {
    this.dataServiceProvider.todoListHandler.subscribe(storageModel => {
      this.todoList = storageModel.items.filter(x => x.todoStatus == ITEM_STATUS.DOING);
    })
  }

  itemClicked(item) {
    const actionSheetModel: ActionSheet = new ActionSheet(item, this.dataServiceProvider);

    const actionSheet = this.actionSheetCtrl.create({
      title: item.todoItem,
      buttons: actionSheetModel.getDoingActionSheet(() => {
        this.itemMoved = true;
        this.itemRemoved = false;

        setTimeout(() => {
          let clonedItem: Item = Object.assign({}, item);
          clonedItem.todoStatus = ITEM_STATUS.DONE;
          this.dataServiceProvider.removeToDoItem(item);
          this.dataServiceProvider.addTodoItem(clonedItem);
        }, 100);

      },
        () => {
          this.itemMoved = false;
          this.itemRemoved = true;
        })
    });

    actionSheet.present();
  }

}
