import { Component, OnInit } from '@angular/core';
import { NavController, ActionSheetController } from 'ionic-angular';
import { Item } from '../../models/item';
import { DataServiceProvider } from '../../providers/data-service/data-service';
import { ITEM_STATUS } from '../../models/todo-status';

@Component({
  selector: 'page-done',
  templateUrl: 'done.html'
})
export class DonePage implements OnInit {

  todoList: Array<Item> = new Array<Item>();
  constructor(public navCtrl: NavController, 
              private dataServiceProvider: DataServiceProvider, 
              public actionSheetCtrl: ActionSheetController) {

  }

  ngOnInit(): void {
    this.dataServiceProvider.todoListHandler.subscribe(storageModel => {
      this.todoList = storageModel.items.filter(x => x.todoStatus == ITEM_STATUS.DONE);
    })
  }

}
