import { Component, OnInit } from '@angular/core';

import { ToDoPage } from '../todo/todo';
import { DoingPage } from '../doing/doing';
import { DonePage } from '../done/done';
import { DataServiceProvider } from '../../providers/data-service/data-service';
import { NotificationServiceProvider } from '../../providers/notification-service/notification-service';
import { Platform } from 'ionic-angular';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage implements OnInit {

  tab1Root = ToDoPage;
  tab2Root = DoingPage;
  tab3Root = DonePage;

  constructor(private dataServiceProvider: DataServiceProvider,
    private notificationService: NotificationServiceProvider,
    private platform: Platform) {

  }

  ngOnInit(): void {
    this.platform.ready().then(source => {
      this.dataServiceProvider.todoListHandler.subscribe(storageModel => {
        this.notificationService.scheduleNotifications(storageModel);
      });
    });
  }
}
