import { Component, OnInit } from '@angular/core';
import { NavController, ActionSheetController, MenuController, ModalController, LoadingController } from 'ionic-angular';
import { DataServiceProvider } from '../../providers/data-service/data-service';
import { PopoverItemActionsComponent } from '../../components/popover-item-actions/popover-item-actions';
import { trigger, transition, state, style, query, stagger, animate, keyframes, group, useAnimation } from '@angular/animations';
import { Item } from '../../models/item';
import { ITEM_STATUS } from '../../models/todo-status';
import { ActionSheet } from '../../models/action-sheet';
import { dropAnimation, itemLeftAnimation } from '../../providers/animations/animations';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { UUID } from 'angular2-uuid';
import { NotificationServiceProvider } from '../../providers/notification-service/notification-service';
import { CalendarModalOptions, CalendarModal, CalendarResult, CalendarComponentOptions } from 'ion2-calendar';
import { DatePipe } from '@angular/common';
import { Moment } from 'moment';
import { LoaderServiceProvider } from '../../providers/loader-service/loader-service';

@Component({
  selector: 'page-todo',
  templateUrl: 'todo.html',
  animations: [
    trigger('dropAnimation', [
      transition('void => *', [
        useAnimation(dropAnimation)
      ])
    ])
  ]
})
export class ToDoPage implements OnInit {

  todoList: Array<Item> = new Array<Item>();
  todoItem: Item = new Item();
  showAddTaskContent = false;
  itemRemoved: boolean = false;
  itemMoved: boolean = false;
  showDatePckr: boolean = false;
  readonly TEXT_DEADLINE = "Deadline (optional)";
  deadlineText: string = this.TEXT_DEADLINE;
  calendarComponentOptions: CalendarComponentOptions = {
    //add if needed
  }
  loading: any;

  constructor(public navCtrl: NavController,
    private modalController: ModalController,
    private dataServiceProvider: DataServiceProvider,
    public actionSheetCtrl: ActionSheetController,
    private menuCtrl: MenuController,
    public datePipe: DatePipe,) {
  }

  ngOnInit(): void {
    this.loadItems();
  }

  loadItems(): any {
    this.dataServiceProvider.todoListHandler.subscribe(storageModel => {
      this.todoList = storageModel.items.filter(x => x.todoStatus == ITEM_STATUS.TODO);
    });
  }

  addItem() {
    this.todoItem.todoStatus = ITEM_STATUS.TODO;
    this.todoItem.id = UUID.UUID();
    if (this.todoItem.todoDeadline) {
      this.todoItem.todoDeadline = new Date(new Date(this.todoItem.todoDeadline).setHours(23, 59, 59));
    }
    this.dataServiceProvider.addTodoItem(this.todoItem);
    this.todoItem = new Item();
    this.deadlineText = this.TEXT_DEADLINE;
  }

  itemClicked(item) {
    this.presentActionSheet(item);
  }

  presentActionSheet(item: Item) {
    const actionSheetModel: ActionSheet = new ActionSheet(item, this.dataServiceProvider);

    const actionSheet = this.actionSheetCtrl.create({
      title: item.todoItem,
      buttons: actionSheetModel.getToDoActionSheet(
        () => {
          this.itemMoved = true;
          this.itemRemoved = false;
          setTimeout(() => {
            let clonedItem: Item = Object.assign({}, item);
            clonedItem.todoStatus = ITEM_STATUS.DOING;
            this.dataServiceProvider.removeToDoItem(item);
            this.dataServiceProvider.addTodoItem(clonedItem);
          }, 100);
        },
        () => {
          this.itemMoved = false;
          this.itemRemoved = true;
        },
        () => {
          this.modalController.create(PopoverItemActionsComponent, {item:item})
            .present();
        }
      )
    });

    actionSheet.present();
  }

  deadlineChanged(item: Item) {
    this.dataServiceProvider.updateToDoItem(item);
  }

  toggleAddTask() {
    this.showAddTaskContent = !this.showAddTaskContent;
  }

  showSettings() {
    this.menuCtrl.open();
  }

  showDatePicker() {
    this.showDatePckr = !this.showDatePckr;
  }

  dateChanged(event: Moment) {
    this.deadlineText = this.datePipe.transform(event.toDate(), 'mediumDate');
    this.showDatePckr = false;
  }
}
