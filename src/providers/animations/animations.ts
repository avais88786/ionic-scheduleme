import { animation, query, animate, keyframes, style } from "@angular/animations";

export var dropAnimation = animation([
    query('#ionItemTodo,#ionIcon1,#ionItemDeadline', style({ opacity: 0 })),
    query('#ionItemTodo,#ionIcon1,#ionItemDeadline',
        animate('0.6s ease-in', keyframes([
            style({ opacity: 0.5, transform: 'translateX(75%)', offset: 0 }),
            style({ opacity: 0.7, transform: 'translateX(-15px)', offset: 0.3 }),
            style({ opacity: 1, transform: 'translateX(0)', offset: 1.0 })
        ])))
])


export var itemLeftAnimation = animation([
    animate('0.5s ease-in', keyframes([
        style({ opacity: 1, transform: "{{ x_1 }}" }),
        style({ opacity: 0.8, transform: "{{ x_2 }}" }),
        style({ opacity: 0.5, transform: "{{ x_3 }}" }),
        style({ opacity: 0.3, transform: "{{ x_4 }}" })
    ]))
], {
        params: {
            x_1: 'translateX(0%)',
            x_2: 'translateX(30%)',
            x_3: 'translateX(50%)',
            x_4: 'translateX(80%)'
        }
})


// trigger('dropAnimation', [
//     transition('void => *', [
//       query('#ionItemTodo,#ionIcon1', style({ opacity: 0 })),
//       group([
//         query('#ionItemTodo,#ionIcon1', group([
//           stagger('100ms', [
//             animate('0.2s ease-in', keyframes([
//               style({ opacity: 0, transform: 'translateX(75%)', offset: 0 }),
//               style({ opacity: 1, transform: 'translateX(0)', offset: 1.0 })
//             ]))])
//         ])),
//         query(':self', group([
//           style({ height: 0 }),
//           animate('0.2s ease-out', style({ height: '*' }))
//         ]))
//       ])
//     ]),
//     transition('* => void', [
//       query(':self', 
//         animate('0.2s ease-in', style({ height: '0px' }))
//       )
//     ])
//   ]),
//   trigger('fly', [
//     transition('* => *', [
//       query('button', style({ opacity: 0 })),
//       query('button',
//         animate('0.5s ease-in', keyframes([
//           style({ opacity: 0.5, transform: 'translateX(-55%)', offset: 0 }),
//           style({ opacity: 1, transform: 'translateX(0)', offset: 1.0 })
//         ])))
//     ])
//   ])