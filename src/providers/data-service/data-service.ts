
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Storage } from '@ionic/storage';
import { Item } from '../../models/item';
import { StorageModel } from '../../models/storage';

@Injectable()
export class DataServiceProvider {

  private todoListSubject: BehaviorSubject<StorageModel> = new BehaviorSubject(new StorageModel());
  todoListHandler: Observable<StorageModel> = this.todoListSubject.asObservable();

  constructor(private storage: Storage) {
    storage.get('storageModel').then(storageModel => {
      if (storageModel) {
        this.todoListSubject.next(storageModel);
      }
    });
  }

  addTodoItem(item: Item) {
    let storageModel = this.todoListSubject.value;
    storageModel.items.push(item);
    this.storage.set('storageModel', storageModel);
    this.todoListSubject.next(storageModel);
  }

  removeToDoItem(item: Item) {
    let storageModel = this.todoListSubject.value;
    const index = storageModel.items.findIndex(x => x.id == item.id);
    storageModel.items.splice(index, 1);
    this.storage.set('storageModel', storageModel);
    this.todoListSubject.next(storageModel);
  }

  updateToDoItem(item: Item) {
    let storageModel = this.todoListSubject.value;
    let itemObject = storageModel.items.find(x => x.id == item.id);
    let clonedItemObj = Object.assign({}, item);
    itemObject = clonedItemObj;
    this.storage.set('storageModel', storageModel);
    this.todoListSubject.next(storageModel);
  }

  updateIntervals(enableNotifications: boolean, todo_reminder: number, doing_interval: string, todo_reminder_hrs: number, todo_reminder_mns: number): any {
    let storageModel = this.todoListSubject.value;
    storageModel.notificationSettings.enableNotifications = enableNotifications;
    storageModel.notificationSettings.TODO_Reminder = todo_reminder == null ? storageModel.notificationSettings.TODO_Reminder : todo_reminder;
    storageModel.notificationSettings.DOING_Interval = doing_interval;
    storageModel.notificationSettings.TODOReminderhour = todo_reminder_hrs;
    storageModel.notificationSettings.TODOReminderMinutes = todo_reminder_mns;
    this.storage.set('storageModel', storageModel);
    this.todoListSubject.next(storageModel);
  }

}
