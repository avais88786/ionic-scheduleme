import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { LoadingController, Loading } from 'ionic-angular';

@Injectable()
export class LoaderServiceProvider {

  //private loaderSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  //loaderStatus: Observable<boolean> = this.loaderSubject.asObservable();
  loader: Loading

  constructor(private loadingCtrl: LoadingController) { }

  public operationStarted(loadingMessage: string) {

    this.operationEnded();
    this.loader = this.loadingCtrl.create({
      content: `Please wait... <br/>${loadingMessage}`
    });
    this.loader.present();
  }

  public operationEnded() {
    if (this.loader) {
      this.loader.dismiss();
    }
  }
}
