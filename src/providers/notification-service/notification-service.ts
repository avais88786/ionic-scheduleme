import { Injectable } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Item } from '../../models/item';
import { NotificationScheduler } from '../../models/notification-scheduler';
import { ToastController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { StorageModel } from '../../models/storage';
import { LoaderServiceProvider } from '../loader-service/loader-service';

/*
  Generated class for the NotificationServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NotificationServiceProvider {

  constructor(private localNotifications: LocalNotifications,
    private toastCtrl: ToastController,
    private platform: Platform,
    private loaderService: LoaderServiceProvider) {


  }

  shouldShowLoader: boolean = false;

  scheduleNotifications(storageModel: StorageModel) {

    this.platform.ready().then(source => {
      this.shouldShowLoader = true;
      if (storageModel == null || storageModel.items.length == 0) {
        return;
      }

      if (source === 'dom' || this.platform.is('core')) {
        new NotificationScheduler(this.localNotifications).scheduleNotifications(storageModel);
      } else {
        setTimeout(() => {
          if (this.shouldShowLoader){
            this.loaderService.operationStarted('Scheduling notifications, taking longer than ususal..');
          }
        }, 2000);
        
        this.localNotifications.getScheduled().then(notifications => {
          var promises = [];
          notifications.forEach(notification => {
            promises.push(this.localNotifications.cancel(notification.id));
          })

          Promise.all(promises).then(results => {
            if (storageModel.notificationSettings.enableNotifications) {
              new NotificationScheduler(this.localNotifications).scheduleNotifications(storageModel);
            }
            return new Promise((function (resolve, reject) {
              resolve(true);
            }));
          }).then(res => {
            this.shouldShowLoader = false;
            this.loaderService.operationEnded();
          });
        })
      }
    });
  }
}
